package com.example.or185014.hersheybar;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by or185014 on 4/29/2016.
 */
public class CameraHandler {
    private Activity mActivity;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int RESULT_OK = 1;

    public CameraHandler(Activity activity){
        mActivity = activity;
        //Disable button if the user has no camera
        if (!hasCamera()){
            Button cameraButton = (Button) mActivity.findViewById(R.id.btnPhoto);
            cameraButton.setEnabled(false);

        }
    }
    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //Take a picture and store Result(image info)
        mActivity.startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == REQUEST_IMAGE_CAPTURE && requestCode == RESULT_OK){
            //Get the photo
            Bundle extras = data.getExtras();
            Bitmap photo = (Bitmap) extras.get("data");
            ImageView imgViewMain = (ImageView) mActivity.findViewById(R.id.imgViewMain);
            imgViewMain.setImageBitmap(photo);
        }
    }

    private boolean hasCamera() {
        return mActivity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);
    }
}
