package com.example.or185014.hersheybar;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by or185014 on 4/29/2016.
 */
public class GeneralUtils {

    private Toast mToast;
    private Context mContext;

    public GeneralUtils(Context context){
        mContext = context;
    }

    public void DisplayToast(String msg) {
        if (mToast != null){
            mToast.cancel();
        }
        mToast = Toast.makeText(mContext, msg, Toast.LENGTH_SHORT);
        mToast.show();
    }
}
