package com.example.or185014.hersheybar;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by or185014 on 4/29/2016.
 */
public class HttpComm {
    private String basePath = "";
    private String route = "";
    private String requestString = "";
    private Context mContext;
    private HttpResponseListener mListener;
    private int mRequestMethod = Request.Method.GET;
    private String mJsonBody = "{}";
    private Map<String, String> mHeaders;
    private Response.Listener<String> mRListener;
    private Response.ErrorListener mErrorListener;
    private Response.Listener<JSONObject> mJSONRListener;
    private Response.ErrorListener mJSONErrorListener;

    public HttpComm(Context context, Activity activity){
        mContext = context;
        try {
            mListener = (HttpResponseListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement HttpResponseListener");
        }
        createResponseListeners();
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public void setRoute(String route){
        //TODO: make sure slashes are added correctly
        this.route = route;
    }

    public void setRequestMethod(int method){
        mRequestMethod = method;
    }

    public void setJsonBody(String jsonBody){
        this.mJsonBody = jsonBody;
    }

    public void stringRequest(){
        // Instantiate the RequestQueue.
        RequestQueue queue = MyRequestQueueSingleton.getInstance(mContext).getRequestQueue();
        requestString = basePath + route;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(mRequestMethod, requestString, mRListener, mErrorListener){
            /*@Override
            public byte[] getBody() throws AuthFailureError
            {
                String body = "some text";
                try
                {
                    return body.getBytes(getParamsEncoding());
                }
                catch (UnsupportedEncodingException uee)
                {
                    throw new RuntimeException("Encoding not supported: "
                            + getParamsEncoding(), uee);
                }
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/xml");
                return headers;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void jsonRequest(){
        // Instantiate the RequestQueue.
        RequestQueue queue = MyRequestQueueSingleton.getInstance(mContext).getRequestQueue();
        requestString = basePath + route;
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject(mJsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Request a string response from the provided URL.
        JsonObjectRequest stringRequest = new JsonObjectRequest(mRequestMethod, requestString, jsonBody , mJSONRListener, mJSONErrorListener);
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public interface  HttpResponseListener{
        void onSuccessfulResponse(String res);
        void onErrorResponse(String error);
    }

    private void createResponseListeners() {
        mRListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // Display the first 500 characters of the response string.
                Log.d("Response:", response);
                mListener.onSuccessfulResponse(response);
            }
        };
        mErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Response:", error.toString());
                mListener.onErrorResponse(error.toString());
            }
        };
        mJSONRListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // Display the first 500 characters of the response string.
                Log.d("Response:", response.toString());
                mListener.onSuccessfulResponse(response.toString());
            }
        };
        mJSONErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String responseBody = "Error with Volley Request";
                try {
                    responseBody = new String( error.networkResponse.data, "utf-8" );
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                Log.d("Response:", responseBody);
                mListener.onErrorResponse(responseBody);
            }
        };
    }
}
