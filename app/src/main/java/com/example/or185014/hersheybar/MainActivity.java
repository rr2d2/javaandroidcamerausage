package com.example.or185014.hersheybar;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements HttpComm.HttpResponseListener {

    private HttpComm http;
    private Toast mToast;
    private String API_KEY = "AIzaSyCuLPdlRR0RTXXvsQ2QXHex-wUUAW8KzxY";
    private String UPLOAD_URL = "http://www.google.com";         //apis.com/upload/storage/v1/b/hershey/o?uploadType=media&name=";
    private GeneralUtils gu;
    private CameraHandler camera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gu = new GeneralUtils(getApplicationContext());
        camera = new CameraHandler(this);

        http = new HttpComm(getApplicationContext(), this);
        http.setBasePath(UPLOAD_URL);//+"test.jpg");
        http.stringRequest();
    }


    @Override
    public void onSuccessfulResponse(String res) {
        Log.d("Response:", res);
        gu.DisplayToast(res);
    }

    @Override
    public void onErrorResponse(String error) {
        Log.d("Error Response:", error);
        gu.DisplayToast(error);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        camera.onActivityResult(requestCode, resultCode, data);
    }

    public void onButtonUpload(View v){
        gu.DisplayToast("Button Upload");
    }

    public void onButtonClearText(View v){
        gu.DisplayToast("Button Clear Text");
    }

    public void onButtonTakePhoto(View v){
        gu.DisplayToast("Button Take Photo");
        camera.takePhoto();
    }
}
